//This file test rotation of 2 Wheel in robot model
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/Twist.h>
#include <math.h>

double leftVel= 0.0, rightVel= 0.0;
double scale= 0.5;
const double DEG_TO_RAD = M_PI/180;


// Received message Callback funtion
void teleopMessageReceived(const geometry_msgs::Twist& msg){
    ROS_INFO_STREAM(std::setprecision(2) << std::fixed
     << "x= " << msg.linear.x << ","
     << "z= " << msg.angular.z);
    if(msg.angular.z==0){
        //robot run Straight
        //if msg.linear.x=0, robot Stop
        rightVel = msg.linear.x/2;
        leftVel = rightVel;
    }
    if(msg.angular.z!=0 && msg.linear.x==0){
        //robot Rotate
        rightVel = msg.angular.z*scale;
        leftVel = -rightVel;
    }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "control_joint");
  ros::NodeHandle n;

  //The node advertises the joint values of the pan-tilt
  ros::Publisher joint_pub = n.advertise<sensor_msgs::JointState>("joint_states", 1);
  ros::Subscriber sub = n.subscribe("teleop_values", 1000, &teleopMessageReceived);

  sensor_msgs::JointState joint_state;
  // message declarations
  joint_state.name.resize(2);
  joint_state.position.resize(2);

  while (ros::ok())
  {
      //update joint_state
      joint_state.header.stamp = ros::Time::now();
      joint_state.name[0] ="Left_Joint";
      joint_state.position[0] = leftVel;
      joint_state.name[1] ="Right_Joint";
      joint_state.position[1] = rightVel;

      //send the joint state
      joint_pub.publish(joint_state);

      ros::spinOnce();
  }
  return(0);
}
