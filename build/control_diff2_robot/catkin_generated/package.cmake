set(_CATKIN_CURRENT_PACKAGE "control_diff2_robot")
set(control_diff2_robot_VERSION "0.0.0")
set(control_diff2_robot_MAINTAINER "tung <tung@todo.todo>")
set(control_diff2_robot_PACKAGE_FORMAT "2")
set(control_diff2_robot_BUILD_DEPENDS )
set(control_diff2_robot_BUILD_EXPORT_DEPENDS )
set(control_diff2_robot_BUILDTOOL_DEPENDS "catkin")
set(control_diff2_robot_BUILDTOOL_EXPORT_DEPENDS )
set(control_diff2_robot_EXEC_DEPENDS )
set(control_diff2_robot_RUN_DEPENDS )
set(control_diff2_robot_TEST_DEPENDS )
set(control_diff2_robot_DOC_DEPENDS )
set(control_diff2_robot_URL_WEBSITE "")
set(control_diff2_robot_URL_BUGTRACKER "")
set(control_diff2_robot_URL_REPOSITORY "")
set(control_diff2_robot_DEPRECATED "")